from django.urls import path, include
from django.contrib import admin
from rest_framework_simplejwt.views import TokenObtainPairView, \
    TokenRefreshView

urlpatterns = [
    path('api/v1/', include('py_code.messages_api.urls')),
    path('api/v1/token/', TokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path('api/v1/token/refresh/', TokenRefreshView.as_view(),
         name='token_refresh'),
    path('admin/', admin.site.urls),
]
