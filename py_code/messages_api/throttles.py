import logging

from rest_framework.throttling import SimpleRateThrottle

logger = logging.getLogger("throttle")


class CreateMessageThrottle(SimpleRateThrottle):
    rate = "10/minute"

    def allow_request(self, request, view):
        throttle_result = super().allow_request(request, view)
        if not throttle_result:
            logger.warning(
                f"Limit on the message creation has been "
                f"exceeded from {self.get_ident(request)} "
                f"({request.user.id, request.user.username} "
                f"{request.data=}"
            )
        return throttle_result

    def get_cache_key(self, request, view):
        return self.get_ident(request)
