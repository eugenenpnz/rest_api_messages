from py_code.messages.celery import app
from py_code.messages_api.models import Message


@app.task
def set_sent_true_to_message(message_id: int):
    message = Message.objects.get(pk=message_id)
    message.sent = True
    message.save()
