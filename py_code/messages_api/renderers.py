from rest_framework_csv.renderers import CSVRenderer


class MessageCsvRenderer(CSVRenderer):
    header = ["id", "header", "body", "sent", "read", "create_datetime"]
