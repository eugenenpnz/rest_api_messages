from datetime import datetime

from django.utils.timezone import utc
from rest_framework import generics, views, status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response

from py_code.messages_api.models import Message
from py_code.messages_api.permissions import IsMessagePublisherOrAdmin
from py_code.messages_api.renderers import MessageCsvRenderer
from py_code.messages_api.serializers import (
    MessageInputSerializer,
    MessageOutputSerializer,
)
from py_code.messages_api.throttles import CreateMessageThrottle
from py_code.messages_api.tasks import set_sent_true_to_message


class MessageCreateView(generics.CreateAPIView):
    """
    Creates a message
    """
    serializer_class = MessageInputSerializer
    permission_classes = (IsAuthenticated,)
    throttle_classes = (CreateMessageThrottle,)

    def create(self, request: Request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST)
        message = serializer.save(user=request.user)
        headers = self.get_success_headers(serializer.data)
        set_sent_true_to_message.delay(message.id)

        return Response(
            data=MessageOutputSerializer(message).data,
            status=status.HTTP_201_CREATED,
            headers=headers,
        )


class ProcessMessageView(views.APIView):
    """
    Allow GET message for all and delete admin or publisher
    """
    permission_classes = (IsMessagePublisherOrAdmin,)

    def get(self, request: Request, pk: int):
        message = get_object_or_404(Message, pk=pk)
        serializer = MessageOutputSerializer(message)
        return Response(serializer.data)

    def delete(self, request: Request, pk: int):
        message = get_object_or_404(Message, pk=pk)
        self.check_object_permissions(request, message)
        message.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class MessageReadStatusUpdate(views.APIView):
    """
    Set read flag
    """
    permission_classes = (IsAuthenticated,)

    def patch(self, request: Request, pk: int):
        message = get_object_or_404(Message, pk=pk)
        message.read = True
        message.update_datetime = datetime.now(utc)
        message.save()
        return Response(status=status.HTTP_200_OK)


class MessagesCsvView(views.APIView):
    """
    Return a generated csv file filled with all messages and headers
        ['id', 'header', 'body', 'sent', 'read', 'create_datetime']
    """
    renderer_classes = [MessageCsvRenderer]
    permission_classes = (IsAdminUser,)

    def get(self, request: Request):
        messages = Message.objects.order_by("create_datetime")
        content = [
            {
                "id": message.id,
                "header": message.header,
                "body": message.body,
                "sent": message.sent,
                "read": message.read,
                "create_datetime": message.create_datetime,
            }
            for message in messages
        ]
        return Response(content)
