from django.db import models
from django.conf import settings


class Message(models.Model):
    header = models.CharField(max_length=64)
    body = models.CharField(max_length=300)
    sent = models.BooleanField(default=False)
    read = models.BooleanField(default=False)
    create_datetime = models.DateTimeField(auto_now_add=True)
    update_datetime = models.DateTimeField(null=True)

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
