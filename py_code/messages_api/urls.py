from django.urls import path
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view

from py_code.messages_api.views import (
    MessageCreateView,
    ProcessMessageView,
    MessageReadStatusUpdate,
    MessagesCsvView,
)

urlpatterns = [
    path(
        "openapi",
        get_schema_view(
            title="messages", description="API for messaging", version="1.0.0"
        ),
        name="openapi-schema",
    ),
    path(
        "swagger-ui/",
        TemplateView.as_view(
            template_name="swagger-ui.html",
            extra_context={"schema_url": "openapi-schema"},
        ),
        name="swagger-ui",
    ),
    path(
        "redoc/",
        TemplateView.as_view(
            template_name="redoc.html",
            extra_context={"schema_url": "openapi-schema"}
        ),
        name="redoc",
    ),
    path("messages/", MessageCreateView.as_view(), name="messages"),
    path("messages/<int:pk>/",
         ProcessMessageView.as_view(),
         name="process-message"
    ),
    path(
        "messages/<int:pk>/read-status/",
        MessageReadStatusUpdate.as_view(),
        name="update-read-status-message",
    ),
    path(
        "messages-list/csv-file/",
        MessagesCsvView.as_view(),
        name="messages-csv-file"
    ),
]
