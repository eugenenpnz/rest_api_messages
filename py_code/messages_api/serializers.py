from rest_framework import serializers

from py_code.messages_api.models import Message


class MessageInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = [
            "header",
            "body",
        ]


class MessageOutputSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = [
            "header",
            "body",
            "sent",
            "read",
            "create_datetime",
            "update_datetime",
        ]
