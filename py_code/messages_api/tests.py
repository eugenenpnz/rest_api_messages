from django.contrib.auth.models import User
from django.core.cache import cache
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from py_code.messages_api.models import Message


class CreateMessageTests(APITestCase):
    def setUp(self):
        self.url = reverse("messages")
        self.data = {"header": "testHeader", "body": "testBody"}
        self.create_limit = 10
        cache.clear()
        self.default_user = User.objects.create_user(
            "user", "user@mail.com", "user")

        user_auth_response = self.client.post(
            reverse("token_obtain_pair"),
            data={"username": "user", "password": "user"}
        )

        self.assertEqual(user_auth_response.status_code, status.HTTP_200_OK)
        jwt_user_token = "Bearer " + user_auth_response.data["access"]
        self.headers = {"Authorization": jwt_user_token}
        self.headers = {"HTTP_AUTHORIZATION": jwt_user_token}

    def test_create_message_unauthorized(self):
        response = self.client.post(self.url, self.data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_message_authorized(self):
        response = self.client.post(self.url, self.data, **self.headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Message.objects.count(), 1)
        self.assertEqual(Message.objects.get().header, "testHeader")
        self.assertEqual(Message.objects.get().body, "testBody")

    def test_block_on_create_from_ip_user(self):
        for i in range(self.create_limit):
            self.client.post(self.url, self.data, **self.headers)
        self.assertEqual(Message.objects.count(), self.create_limit)
        failed_response = self.client.post(self.url, self.data, **self.headers)
        self.assertEqual(failed_response.status_code,
                         status.HTTP_429_TOO_MANY_REQUESTS)


class ProcessMessageTests(APITestCase):
    def setUp(self):
        self.publisher = User.objects.create_user(
            "publisher", "publisher@mail.com", "publisher"
        )

        auth_request = self.client.post(
            reverse("token_obtain_pair"),
            data={"username": "publisher", "password": "publisher"},
        )

        self.jwt_publisher = "Bearer " + auth_request.data["access"]

        self.user = User.objects.create_user("user", "user@mail.com", "user")

        auth_request = self.client.post(
            reverse("token_obtain_pair"),
            data={"username": "user", "password": "user"}
        )

        self.jwt_user = "Bearer " + auth_request.data["access"]

        self.message = Message.objects.create(
            header="header", body="body", user=self.publisher
        )

    def test_successful_read_message(self):
        url = reverse("process-message", kwargs={"pk": self.message.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_authorized_not_admin_or_publisher_delete(self):
        url = reverse("process-message", kwargs={"pk": self.message.pk})
        response = self.client.delete(url,
                                      **{"HTTP_AUTHORIZATION": self.jwt_user})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_publisher_delete(self):
        url = reverse("process-message", kwargs={"pk": self.message.pk})
        headers = {"HTTP_AUTHORIZATION": self.jwt_publisher}
        response = self.client.delete(url, **headers)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class UpdateReadStatusTest(APITestCase):
    def setUp(self):
        user = User.objects.create_user(
            username="test_username", password="test_password"
        )

        self.message = Message.objects.create(
            header="header", body="body", user=user)
        self.url = reverse("update-read-status-message",
                           kwargs={"pk": self.message.pk})
        auth_response = self.client.post(
            reverse("token_obtain_pair"),
            data={"username": "test_username", "password": "test_password"},
        )
        self.assertEqual(auth_response.status_code, status.HTTP_200_OK)
        self.jwt_token = "Bearer " + auth_response.data["access"]
        self.headers = {"HTTP_AUTHORIZATION": self.jwt_token}

    def test_authorized_update_status(self):
        response = self.client.patch(self.url, **self.headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_unauthorized_update_status(self):
        response = self.client.patch(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class GenerateCsvTest(APITestCase):
    def setUp(self):
        User.objects.create_superuser(username="admin", password="admin")
        self.url = reverse("messages-csv-file")
        auth_response = self.client.post(
            reverse("token_obtain_pair"),
            data={"username": "admin", "password": "admin"},
        )
        self.assertEqual(auth_response.status_code, status.HTTP_200_OK)
        jwt_token = "Bearer " + auth_response.data["access"]
        self.headers = {"HTTP_AUTHORIZATION": jwt_token}

    def test_authorized_generate(self):
        response = self.client.get(self.url, **self.headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_unauthorized_generate(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
