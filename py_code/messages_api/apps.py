from django.apps import AppConfig


class MessagesApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'py_code.messages_api'
